# RetroBot
## Running
Execute `npm start` to start the API. You can also run `npm run watch-api-start` to start the API with file watches and reloading.

## Setup Repo
### Network tunnel via [ngrok (https://www.ngrok.com)](https://www.ngrok.com)

Find your active tunnel by going to https://dashboard.ngrok.com/status, executing `npm run tunnel-get-url`, or going to http://localhost:4040/status.